import {Schema, model, Types} from 'mongoose'

const schema = new Schema({
    text: {type: String, required: true},
    userId: {type: Types.ObjectId, required: true, ref: 'User'},
    createdAt: {type: Date, default: Date.now},
    editedAt: {type: Date}
});

export default model('Messages', schema);