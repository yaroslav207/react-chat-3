import {Router} from 'express';
import User from '../models/User'
import {encrypt, cryptCompare} from '../helpers/crypt/crypt';
import {createToken} from "../helpers/token/token";
import auth from '../middleware/auth.middleware'


const router = Router();

router
    .post('/login', async (req, res) => {
        const {login, password} = req.body;
        const user = await User.findOne({login: login});
        if (!user || !cryptCompare(password, user.password)) {
            res.status(400).json({message: 'Неверный логин или пароль'})
        }
        res.status(201).json({
            user: {
                id: user.id,
                admin: user.admin,
                name: user.name
            },
            token: createToken({id: user.id})
        })
    })
    .post('/registration', async (req, res) => {
        try {
            const {login, password} = req.body;
            const candidate = await User.findOne({login: login});
            if (candidate) {
                res.status(400).json({message: 'Такой пользователь уже существует'});
            }

            const user = new User({login: login, password: await encrypt(password)});
            await user.save();

            res.status(200).json({userId: user.id, token: createToken({id: user.id})})
        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так, попробуйте снова ${e}`})
        }
    })
    .get('/user', auth,  async (req, res) => {
        try {
            res.status(200).json(req.user)
        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так, попробуйте снова ${e}`})
        }
    });

export default router;
