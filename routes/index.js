import authRoutes from "./authRoutes";
import messageRoutes from "./messageRoutes";
import userRoutes from "./userRoutes";


export default app => {
  app.use("/api/auth", authRoutes);
  app.use("/api/users", userRoutes);
  app.use("/api/messages", messageRoutes);
};
