import {Router} from "express";
import User from "../models/User";
import admin from '../middleware/admin.middleware'
import {encrypt} from "../helpers/crypt/encrypt/encrypt.helper";
import {createToken} from "../helpers/token/create-token/create-token.helper";

const router = Router();

router
    .get("/", async (req, res) => {
        try {
            const users = await User.find({}, ['login', 'admin', 'avatar'])
            if (!users) {
                res.status(404).json({message: 'Не найдено'})
            }
            res.status(200).json(users);
        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так, попробуйте снова ${e}`})
        }
    })
    .get("/:id", async (req, res) => {
        try {
            const user = await User.findOne({_id: req.params.id}, ['login', 'admin', 'avatar']);
            if (!user) {
                res.status(404).json({message: 'Не найдено'})
            }
            res.status(200).json(user)
        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так, попробуйте снова ${e}`})
        }
    })
    .post("/", admin, async (req, res) => {
        try {
            const {login, password} = req.body;
            const candidate = await User.findOne({login: login});
            if (candidate) {
                res.status(400).json({message: 'Такой пользователь уже существует'});
            }

            const user = new User({...req.body, password: await encrypt(password)});
            await user.save();

            res.status(200).json({user});
        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так, попробуйте снова ${e}`})
        }
    })
    .put("/:id",
        admin,
        async (req, res) => {
            try {

                const user = await User.updateOne(
                    {_id: req.params.id},
                    {...req.body},
                    {upsert: true}
                );
                if (!user) {
                    res.status(404).json({message: 'Не найдено'})
                }
                const updatedUser = await User.findOne({_id: req.params.id}, ['login', 'admin', 'avatar']);
                res.status(200).json(updatedUser)
            } catch (e) {
                res.status(500).json({message: `Что-то пошло не так, попробуйте снова ${e}`});
            }
        })
    .delete("/:id",
        admin,
        async (req, res) => {
            try {
                const user = await User.deleteOne({_id: req.params.id});
                if (!user) {
                    res.status(404).json({message: 'Не найдено'})
                }

                res.status(200).json({id: req.params.id})
            } catch (e) {
                res.status(500).json({message: `Что-то пошло не так, попробуйте снова ${e}`})
            }
        });

export default router;
