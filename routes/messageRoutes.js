import {Router} from "express";
import Message from "../models/Messages";
import auth from '../middleware/auth.middleware'
import permissionForMessages from '../middleware/permissionsMessage.middleware'


const router = Router();

router
    .get("/", auth, async (req, res) => {
        try {
            const messages = await Message.find({});
            if (!messages) {
                res.status(404).json({message: 'Не найдено'})
            }
            res.status(200).json(messages)
        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так, попробуйте снова ${e}`})
        }
    })
    .get("/:id", auth, async (req, res) => {
        try {
            const message = await Message.findOne({_id: req.params.id})
            if (!message) {
                res.status(404).json({message: 'Не найдено'})
            }
            res.status(200).json(message)
        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так, попробуйте снова ${e}`})
        }
    })
    .post("/", auth, async (req, res) => {
        const newMessageBody = {
            text: req.body.text,
            userId: req.user.id,
        };

        const message = new Message(newMessageBody);
        await message.save()

        const newMessage = await Message.findOne({_id: message._id});
        res.status(201).json(newMessage);
    })
    .put("/:id",
        [
            auth,
            permissionForMessages
        ],
        async (req, res) => {
            try {
                const message = await Message.updateOne(
                    {_id: req.params.id},
                    {...req.body, editedAt: new Date()},
                    {upsert: true}
                );
                if (!message) {
                    res.status(404).json({message: 'Не найдено'})
                }
                const updatedMessage = await Message.findOne({_id: req.params.id});
                res.status(200).json(updatedMessage)
            } catch (e) {
                res.status(500).json({message: `Что-то пошло не так, попробуйте снова ${e}`});
            }
        })
    .delete("/:id",
        [
            auth,
            permissionForMessages
        ],
        async (req, res) => {
            try {
                const message = await Message.deleteOne({_id: req.params.id});
                if (!message) {
                    res.status(404).json({message: 'Не найдено'})
                }

                res.status(200).json({id: req.params.id})
            } catch (e) {
                res.status(500).json({message: `Что-то пошло не так, попробуйте снова ${e}`})
            }
        });

export default router;
