import express from 'express'
import config from 'config'
import routes from './routes/'
import mongoose from 'mongoose'

const PORT = config.get('PORT') || 5000;

const app = express();
app.use(express.json({extended: true}));


routes(app);

app.get("*", (req, res) => {
    res.redirect("/login");
});

const start = async () => {
    try {
        await mongoose.connect(config.get('mongoUri'), {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useCreateIndex: true
            }
        )
    } catch (e) {
        console.log('Server error ' + e.message)
        process.exit(1)
    }
};

start();

app.listen(PORT, () => {
    console.log(`App has been started on port ${PORT} ...`)
});

