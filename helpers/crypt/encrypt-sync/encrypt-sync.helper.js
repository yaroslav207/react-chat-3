import { hashSync } from 'bcrypt';
import config from 'config';

const encryptSync = data => hashSync(data, config.get('jwtSecret'));

export { encryptSync };
