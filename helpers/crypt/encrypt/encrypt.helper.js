import { hash } from 'bcrypt';
import config from 'config';

const encrypt = data => hash(data, config.get('salt'));

export { encrypt };
