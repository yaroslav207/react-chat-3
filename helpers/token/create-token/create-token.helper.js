import jwt from 'jsonwebtoken';
import config from 'config'

const createToken = data => jwt.sign(data, config.get('jwtSecret'), { expiresIn: '1h' });

export { createToken };
