import Message from "../models/Messages";

export default async (req, res, next) => {
    if (req.method === 'OPTIONS') {
        return next();
    }
    try {
        const {id: currentUserId} = req.user;

        const {userId} = await Message.findOne({_id: req.params.id});

        if(currentUserId.toString() !== userId.toString()){
            throw new Error()
        }
        next()
    } catch (e) {
        res.status(403).json({message: 'Нет доступа'});
    }
};