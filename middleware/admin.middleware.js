import jwt from 'jsonwebtoken';
import config from 'config';
import User from '../models/User';

export default async (req, res, next) => {
    if (req.method === 'OPTIONS') {
        return next();
    }
    try {
        const token = req.headers.authorization.split(' ')[1];
        if (!token) {
            return res.status(401).json({message: 'Нет авторизации'})
        }
        const decoded = jwt.verify(token, config.get('jwtSecret'));

        const {admin} = await User.findOne({_id: decoded.id});
        if(!admin){
            res.status(401).json({message: 'Нет доступа'});
        }
        next()
    } catch (e) {
        res.status(401).json({message: 'Нет авторизации'});
    }
};