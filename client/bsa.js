import Chat from './src/pages/chat/chat'
import { messagesReducer as rootReducer } from './src/store/root-reducer';

export default {
    Chat,
    rootReducer,
}