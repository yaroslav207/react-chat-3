import {createReducer} from '@reduxjs/toolkit';
import {
    addUser
} from './actions'

const initialState = {
    users: [],
    preloader: true,
};



const reducer = createReducer(initialState, builder => {
    builder.addCase('LOAD_USERS', (state, action) => {
        const {users} = action.payload;

        state.users = users;
        state.preloader = false;
    });
    builder.addCase(addUser, (state, action) => {
        const {user} = action.payload;
        state.users = [...state.users, user];
    });
    builder.addCase('DELETE_USER', (state, action) => {
        const {id} = action.payload;

        state.users = state.users.filter(user => user._id !== id)
    });
    builder.addCase('EDIT_USER', (state, action) => {
        const {id, text, editedAt} = action.payload;

        state.users = state.users.map(user => {
            if (user.id === id) {
                user.text = text;
                user.editedAt = editedAt;
            }
            return user
        });
    });
});

export {reducer}
