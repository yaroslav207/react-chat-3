import {createAction} from '@reduxjs/toolkit';
import { user as userService } from '../../services/services';

const setUsers = createAction('LOAD_USERS', (users) => ({
    payload: {
        users
    }
}));

const addUser = createAction('ADD_USER', (user) => ({
    payload: {
      user
    }
}));

const deleteUser = createAction('DELETE_USER', (id) => ({
  payload: {
    id
  }
}));

const editUser = createAction('EDIT_USER', ({id, text}) => ({
  payload: {
    id,
    text,
    editedAt: new Date().toISOString()
  }
}));

const setIdEditUser = createAction('SET_ID_EDIT_USER', (id) => ({
  payload: {
    id
  }
}));

const loadUsers = () => async dispatch => {
    console.log('users')
    const users = await userService.getUsers();

    dispatch(setUsers(users));
};

const applyUser = userBody => async dispatch => {
    const user = await userService.addUser(userBody);

    dispatch(addUser(user))
};

const removeUser = userId => async dispatch => {
    const { id } = await userService.deleteUser(userId);

    dispatch(deleteUser(id));
};

const updateUser = (userId, payload) => async dispatch => {
    const user = await userService.editUser(userId, {...payload});

    dispatch(editUser(user));
};

export {setUsers, addUser, deleteUser, editUser, setIdEditUser, loadUsers, applyUser, removeUser, updateUser};
