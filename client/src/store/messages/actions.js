import {createAction} from '@reduxjs/toolkit';
import {message as messageService} from '../../services/services';

const setMessages = createAction('LOAD_MESSAGES', (messages) => ({
    payload: {
        messages
    }
}));

const addMessage = createAction('ADD_MESSAGE', (message) => ({
    payload: {
        message
    }
}));

const deleteMessage = createAction('DELETE_MESSAGE', (id) => ({
    payload: {
        id
    }
}));

const editMessage = createAction('EDIT_MESSAGE', ({_id, text, editedAt}) => ({
    payload: {
        _id,
        text,
        editedAt
    }
}));

const setIdEditMessage = createAction('SET_ID_EDIT_MESSAGE', (id) => ({
    payload: {
        id
    }
}));

const loadMessages = () => async dispatch => {
    const messages = await messageService.getMessages();

    dispatch(setMessages(messages));
};

const applyMessage = messageBody => async dispatch => {
    const message = await messageService.addMessage(messageBody);

    dispatch(addMessage(message))
};

const removeMessage = messageId => async dispatch => {
    const {id} = await messageService.deleteMessage(messageId);

    dispatch(deleteMessage(id));
};

const updateMessage = (messageId, payload) => async dispatch => {
    const message = await messageService.editMessage(messageId, {text: payload});

    dispatch(editMessage(message));
};

export {
    setMessages,
    addMessage,
    deleteMessage,
    editMessage,
    setIdEditMessage,
    loadMessages,
    applyMessage,
    removeMessage,
    updateMessage
};
