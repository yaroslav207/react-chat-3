import {createReducer} from '@reduxjs/toolkit';
import {
    addMessage
} from './actions'

const initialState = {
    messages: [],
    preloader: true,
};

const reducer = createReducer(initialState, builder => {
    builder.addCase('LOAD_MESSAGES', (state, action) => {
        const {messages} = action.payload;

        state.messages = messages;
        state.preloader = false;
    });
    builder.addCase(addMessage, (state, action) => {
        const {message} = action.payload;
        state.messages = [...state.messages, message];
    });
    builder.addCase('DELETE_MESSAGE', (state, action) => {
        const {id} = action.payload;

        state.messages = state.messages.filter(message => message._id !== id)
    });
    builder.addCase('EDIT_MESSAGE', (state, action) => {
        const {_id, text, editedAt} = action.payload;

        state.messages = state.messages.map(message => {
            if (message._id === _id) {
                message.text = text;
                message.editedAt = editedAt;
            }
            return message
        });
    });
});

export {reducer}
