import { configureStore } from '@reduxjs/toolkit';
import {messagesReducer, profileReducer, editMessageReducer, userReducer, editUserReducer} from './root-reducer';

const store = configureStore( {
    reducer: {
        message: messagesReducer,
        profile: profileReducer,
        editMessage: editMessageReducer,
        user: userReducer,
        editUser: editUserReducer
    }
});

export default store;
