import {createAction} from '@reduxjs/toolkit';
import { auth as authService } from '../../services/services';
import { storage as storageService } from "../../services/services";

const setUser = createAction('SET_USER', (user) => ({
    payload: {
        user
    }
}));

const login = request => async dispatch => {
    try {
        const { user, token } = await authService.login(request);

        storageService.setItem('token', token);
        dispatch(setUser(user));
    } catch (e) {
        console.log(e)
    }
};

const logout = () => dispatch => {
    try {
        storageService.removeItem('token');
        dispatch(setUser(null));
    } catch (e) {
        console.log(e)
    }
};

const loadCurrentUser = () => async dispatch => {
    try {
        const user = await authService.getCurrentUser();
        dispatch(setUser(user));
    } catch (e) {
        dispatch(logout());
        console.log(e)
    }
};


export {setUser, login, logout, loadCurrentUser};
