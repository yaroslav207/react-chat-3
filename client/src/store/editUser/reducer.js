import {createReducer} from '@reduxjs/toolkit';

const initialState = {
    user: null
};

const reducer = createReducer(initialState, builder => {
    builder.addCase('SET_EDIT_USER', (state, action) => {
        const {user} = action.payload;

        state.user = user;
    });
});

export {reducer}
