import {createAction} from '@reduxjs/toolkit';
import { user as userService } from '../../services/services';


const setEditUser = createAction('SET_EDIT_USER', (user) => ({
    payload: {
        user
    }
}));

const loadEditUser = id => async dispatch => {
    try {
        const user = await userService.getUser(id);

        dispatch(setEditUser(user));
    } catch (e) {
        console.log(e)
    }
};

const deleteEditUser = () => dispatch => {
    try {
        dispatch(setEditUser(null));
    } catch (e) {
        console.log(e)
    }
};


export {setEditUser, loadEditUser, deleteEditUser};
