import {createAction} from '@reduxjs/toolkit';
import { message as messageService } from '../../services/services';


const setEditMessage = createAction('SET_EDIT_MESSAGE', (message) => ({
    payload: {
        message
    }
}));

const loadEditMessage = id => async dispatch => {
    try {
        const message = await messageService.getMessage(id);

        dispatch(setEditMessage(message));
    } catch (e) {
        console.log(e)
    }
};

const deleteEditMessage = () => dispatch => {
    try {
        dispatch(setEditMessage(null));
    } catch (e) {
        console.log(e)
    }
};


export {setEditMessage, loadEditMessage, deleteEditMessage};
