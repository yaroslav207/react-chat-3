import {createReducer} from '@reduxjs/toolkit';

const initialState = {
    message: null
};

const reducer = createReducer(initialState, builder => {
    builder.addCase('SET_EDIT_MESSAGE', (state, action) => {
        const {message} = action.payload;

        state.message = message;
    });
});

export {reducer}
