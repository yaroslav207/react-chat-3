import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useParams, useHistory} from 'react-router-dom';
import {loadEditMessage, deleteEditMessage, updateMessage} from "../../store/actions";
import Preloader from "../../components/preloader/preloader";

function MessageEditor() {
    const {id} = useParams();
    const dispatch = useDispatch();
    const history = useHistory();
    const message = useSelector(state => state.editMessage.message);


    const [editText, setEditText] = useState(message?.text);

    const onCloseEditPage = () => {
        dispatch(deleteEditMessage());
        history.push('/')
    };

    const onSubmitEdit = () => {
        if(!editText){
            return
        }
        dispatch(updateMessage(id, editText));
        history.push('/');
    };

    useEffect(() => {
        dispatch(loadEditMessage(id))
    }, []);

    useEffect(() => {
        console.log(message);
        setEditText(message?.text);
    }, [message]);

    return (
        editText
            ? <div className="message-editor">
                <div className="message-editor__block">
                <textarea name="" id="" cols="50" rows="20" value={editText} onChange={(e) => setEditText(e.target.value)}
                          className="edit-message-input"/>
                    <div className="menu-editor">
                        <button className="edit-message-close" onClick={onCloseEditPage}>CLOSE
                        </button>
                        <button className="edit-message-button" onClick={onSubmitEdit}>SUBMIT
                        </button>
                    </div>
                </div>
            </div>
            : <Preloader/>
    )
}


export default MessageEditor;