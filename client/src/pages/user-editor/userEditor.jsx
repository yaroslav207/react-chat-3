import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useParams, useHistory} from 'react-router-dom';

import {deleteEditUser, loadEditUser, updateUser} from "../../store/actions";
import Preloader from "../../components/preloader/preloader";

function UserEditor() {
    const {id} = useParams();
    const dispatch = useDispatch();
    const history = useHistory();
    const {user} = useSelector(state => state.editUser);


    const [editUser, setEditUser] = useState(user);

    const onCloseEditPage = () => {
        dispatch(deleteEditUser());
        history.push('/user-list/')
    };

    const onSubmitEdit = () => {
        if(!editUser){
            return
        }
        dispatch(updateUser(id, editUser));
        history.push('/user-list/');
    };

    const handlerChangeForm = (e) => {
        let value;
        if(e.target.type === 'checkbox'){
            value = e.target.checked
        } else {
            value = e.target.value
        }
        setEditUser({
            ...editUser,
            [e.target.name]: value
        })
    };

    useEffect(() => {
        dispatch(loadEditUser(id))
    }, []);

    useEffect(() => {
        setEditUser(user);
    }, [user]);

    return (
        editUser
            ? <div className="user-editor">
                <div className="user-editor__block">
                        
                    <div className="menu-editor">
                        <input type="text" name="login" value={editUser.login} onChange={handlerChangeForm}/>
                        <input type="text" name="avatar" value={editUser.avatar} onChange={handlerChangeForm}/>
                        <input type="checkbox" name="admin" checked={editUser.admin}  onChange={handlerChangeForm}/>
                        <button className="edit-user-close" onClick={onCloseEditPage}>CLOSE
                        </button>
                        <button className="edit-user-button" onClick={onSubmitEdit}>SUBMIT
                        </button>
                    </div>
                </div>
            </div>
            : <Preloader/>
    )
}


export default UserEditor;