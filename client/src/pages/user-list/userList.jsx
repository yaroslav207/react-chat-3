import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from 'react-router-dom'

import UserListItem from '../../components/user-list-item/user-list-item'
import {updateUser, removeUser, applyUser, loadUsers} from "../../store/actions";
import Preloader from "../../components/preloader/preloader";

import './style.css';

function UserList() {
    const dispatch = useDispatch();
    const history = useHistory();
    const {users, preloader} = useSelector(state => state.user);
    const myUser = useSelector(state => state.profile.user);


    const toggleEditUserPage = (id) => {
        history.push(`/edit-user/${id}`)
    };

    const handleDeleteUser = (id) => {
        dispatch(removeUser(id))
    };

    const handleCreateUser = () => {
        dispatch(applyUser)
    };

    useEffect(() => {
        if (!myUser || !myUser.admin) {
            history.push('/')
        }
    }, [myUser]);

    useEffect(() => {
        dispatch(loadUsers())
    },[]);

    return (
        preloader
            ? <Preloader/>
            : <div className="user-list">
                <ul>
                    {users?.map((user) => (
                        <UserListItem
                            key={user._id}
                            user={user}
                            onDeleteUser={handleDeleteUser}
                            onEditUser={toggleEditUserPage}
                        />
                    ))}
                </ul>
            </div>
    );
}

export default UserList;