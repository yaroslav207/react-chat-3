import React, {useEffect} from "react";
import {useDispatch, useSelector} from 'react-redux';
import {useHistory} from 'react-router-dom'

import Header from "../../components/header/header";
import MessageList from "../../components/message-list/message-list";
import MessageInput from "../../components/message-input/message-input";
import Preloader from "../../components/preloader/preloader";


import {formatDateLastMessage} from "../../helpers/date/date.helpers";
import {loadMessages, applyMessage, editMessage, removeMessage, loadUsers} from "../../store/actions";

import './style.css'

function Chat({myUser}) {
    const dispatch = useDispatch();
    const history = useHistory()
    const messages = useSelector(state => state.message.messages);
    const users = useSelector(state => state.user.users);
    const preloader = useSelector(state => state.message.preloader);

    const getUserCount = (messages) => {
        const result = [];
        messages.forEach((message) => {
            if (!result.includes(message.userId)) {
                result.push(message.userId);
            }
        });

        return result.length
    };

    const getDateLastMessage = (messages) => {
        if(messages.length === 0){
            return
        }
        let result = messages[0].createdAt;
        messages.forEach(message => {
            if (message.createdAt > result) {
                result = message.createdAt;
            }
        });

        return result
    };

    const getLastMyMessage = (messages) => {
        console.log(myUser);
        return [...messages].reverse().find(message => message.userId === myUser.id)
    };

    const handleDeleteMessage = (id) => {
        dispatch(removeMessage(id))
    };

    const handleAddMessage = (messageText) => {
        if (!messageText) {
            return
        }
        const newMessage = {
            text: messageText,
        };
        dispatch(applyMessage(newMessage));
    };

    const showEditPage = (id) => {
        history.push(`/edit-message/${id}`)

    };

    useEffect(() => {
        dispatch(loadUsers());
        dispatch(loadMessages());
    }, []);

    const handleEditLastMessage = (e) => {
        if (e.code === 'ArrowUp') {
            const message = getLastMyMessage(messages);
            showEditPage(message._id);
        }
    };

    useEffect(() => {
        if (!preloader) {
            document.body.addEventListener("keydown", handleEditLastMessage);
        }
        return () => {
            document.body.removeEventListener("keydown", handleEditLastMessage);
        }
    }, [messages, users]);

    return (!preloader)
        ? (
            <div className="chat">
                <Header
                    usersCount={getUserCount(messages)}
                    messageCount={messages.length}
                    lastMessageDate={formatDateLastMessage(getDateLastMessage(messages))}
                />
                <MessageList
                    messages={messages}
                    users={users}
                    userId={myUser.id}
                    deleteMessage={handleDeleteMessage}
                    showEditPage={showEditPage}
                />
                <MessageInput
                    addMessage={handleAddMessage}
                />
            </div>
        )
        : <Preloader/>
}

export default Chat;