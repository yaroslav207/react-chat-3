import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {login} from '../../store/actions'

function LoginPage() {
    const [form, setForm] = useState({login: '', password: ''});

    const dispatch = useDispatch();

    const handlerChangeForm = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    };
    const onSubmitForm = () => {
        dispatch(login(form))
    };
    return (
        <div className="login-page">
            <div className="login-page__form">
                <input type="text" name="login" onChange={handlerChangeForm}/>
                <input type="text" name="password" onChange={handlerChangeForm}/>
                <button className="submit-button" onClick={onSubmitForm}>LOGIN</button>
            </div>
        </div>
    );
}

export default LoginPage;