import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {applyUser} from '../../store/actions';
import {useHistory} from 'react-router-dom'
import './style.css'

function CreateUser() {
    const [form, setForm] = useState({
        login: '',
        password: '',
        avatar: '',
        admin: false
    });

    const history = useHistory();
    const dispatch = useDispatch();

    const handlerChangeForm = (e) => {
        let value;
        if(e.target.type === 'checkbox'){
            value = e.target.checked
        } else {
            value = e.target.value
        }
        setForm({
            ...form,
            [e.target.name]: value
        })
    };
    const onSubmitForm = () => {
        dispatch(applyUser(form))
        history.push('/user-list/')
    };
    return (
        <div className="login-page">
            <div className="login-page__form">
                <label>
                    <span>login</span>
                    <input type="text" name="login" value={form.login} onChange={handlerChangeForm}/>
                </label>
                <label>
                    <span>password</span>
                <input type="password" name="password" value={form.password} onChange={handlerChangeForm}/>
                </label>
                <label>
                    <span>avatar</span>
                <input type="text" name="avatar" value={form.avatar} onChange={handlerChangeForm}/>
                </label>
                <label>
                    <span>admin</span>
                <input type="checkbox" name="admin" checked={form.admin}  onChange={handlerChangeForm}/>
                </label>
                <button className="submit-button" onClick={onSubmitForm}>CREATE</button>
            </div>
        </div>
    );
}

export default CreateUser;