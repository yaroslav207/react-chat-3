import React, {useEffect} from "react";
import {Switch, Route, useHistory} from "react-router-dom"
import {useDispatch, useSelector} from "react-redux";
import {logout, loadCurrentUser} from "../store/actions";

import Chat from "../pages/chat/chat";
import LoginPage from "../pages/login-page/loginPage";
import MessageEditor from "../pages/message-editor/messageEditor";
import UserList from "../pages/user-list/userList";
import UserEditor from "../pages/user-editor/userEditor";
import AdminNav from "./admin-nav/adminNav";
import CreateUser from "./create-user/createUser";

import logo from "../images/bingo.png";


function App() {
    const user = useSelector(state => state.profile.user);
    const dispatch = useDispatch();
    const history = useHistory();

    const token = localStorage.getItem('token');

    const onClickLogout = () => {
        dispatch(logout())
    };

    useEffect(() => {
        if (token && !user) {
            dispatch(loadCurrentUser())
        }
        if(user?.admin){
            history.push('/user-list/')
        }
    }, [user]);
    return (
        <div className="App">
            <div className="app__header">
                <img src={logo} className="logo" alt="logo"/>
                {user?.admin ? <AdminNav /> : null}
                { user ? <button className="logout-button" onClick={onClickLogout}>Logout</button> : null }
            </div>
            {
                user
                    ?
                    <Switch>
                        <Route path='/' exact render={() => <Chat myUser={user}/>} />
                        <Route path='/edit-message/:id' exact  component={MessageEditor} />
                        <Route path='/user-list/' exact component={UserList} />
                        <Route path='/edit-user/:id' exact component={UserEditor} />
                        <Route path='/create-user/' exact component={CreateUser} />
                    </Switch>
                    : <LoginPage/>
            }
        </div>
    );
}

export default App;
