import React, {useState} from "react";
import './style.css';


function MessageInput({ addMessage }) {
    const [messageText, setMessageText] = useState('');

    const onAddMessage = () => {
        addMessage(messageText);
        setMessageText('');
    };

    return (
        <div className="message-input">
            <input type="text" value={messageText} className="message-input-text" onChange={(e) => setMessageText(e.target.value)} />
            <button className="message-input-button" onClick={onAddMessage}>SEND</button>
        </div>
    );
}

export default MessageInput;