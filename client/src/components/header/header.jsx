import React from "react";
import './style.css'

function Header({usersCount, messageCount, lastMessageDate}) {
    return (
        <div className="header">
            <div className="header-title">Chat1</div>
            <div className="header-users-count">{usersCount}</div><span>  participants</span>
            <div className="header-messages-count">{messageCount}</div><span> messages</span>
            <div className="header-last-message-date-block"><span>last messages at </span><div className="header-last-message-date">{lastMessageDate}</div></div>
        </div>
    );
}

export default Header;