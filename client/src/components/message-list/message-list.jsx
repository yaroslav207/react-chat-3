import React from "react";
import Message from "../message/message";
import OwnMessage from "../own-message/own-message";
import './style.css';
import {formatDataForDivider, formatDataForMessage, parseDay} from "../../helpers/date/date.helpers";

function MessageList({messages = [], users = [], userId, deleteMessage, showEditPage}) {

    let currentDay;
    const todayNumber = new Date().getDate();

    const getUser = (id) => {
        return users.find(user => user._id === id)
    };

    const dividerMessage = (createdAt) => {
        const messageDay = parseDay(createdAt);
        const dividerText = (+messageDay === +todayNumber)
            ? 'Today'
            : (messageDay === (todayNumber - 1))
                ? 'Yesterday'
                : formatDataForDivider(createdAt);
        if (messageDay !== currentDay) {
            currentDay = messageDay;
            return <div className="messages-divider">{dividerText}</div>;
        }
        return null;
    };

    return (
        <div className="message-list">
            {messages.map(message => {
                const formatCreatedAt = formatDataForMessage(message.createdAt);
                const formatEditedAt = formatDataForMessage(message.editedAt);
                return (message.userId === userId)
                    ? <React.Fragment key={message._id}>
                        {dividerMessage(message.createdAt)}
                        <OwnMessage
                            messageInfo={message}
                            user={getUser(message.userId)}
                            handleDeleteMessage={deleteMessage}
                            handleEditMessageId={showEditPage}
                            formatCreatedAt={formatCreatedAt}
                            formatEditedAt={formatEditedAt}
                        />
                    </React.Fragment>
                    : <React.Fragment key={message._id}>
                        {dividerMessage(message.createdAt)}
                        <Message
                            messageInfo={message}
                            user={getUser(message.userId)}
                            formatCreatedAt={formatCreatedAt}
                            formatEditedAt={formatEditedAt}
                        />
                    </React.Fragment>

            })}

        </div>
    );
}

export default MessageList;