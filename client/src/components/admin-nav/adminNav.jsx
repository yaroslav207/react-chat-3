import React from "react";
import {Link} from "react-router-dom";



function AdminNav() {
    return (
        <nav className="admin-nav">
            <Link to='/'><span className="link">CHAT</span></Link>
            <Link to='/create-user/'><span className="link">CREATE USER</span></Link>
            <Link to='/user-list/'><span className="link">USER LIST</span></Link>
        </nav>
    );
}

export default AdminNav;