import React, {useState} from "react";
import './style.css';


function UserListItem({ user, onEditUser, onDeleteUser}) {

    return (
        <li className="user-list__item">
            <div>{user._id}</div>
            <div><img className="user-list__avatar" src={user.avatar} alt="avatar"/></div>
            <div>{user.login}</div>
            <div>{user.admin? "Yes": "No"}</div>
            <div>
                <button className="user-list__button" onClick={() => onEditUser(user._id)}>EDIT</button>
                <button className="user-list__button" onClick={() => onDeleteUser(user._id)}>DELETE</button>
            </div>
        </li>
    );
}

export default UserListItem;