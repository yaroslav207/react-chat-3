import React, {useEffect, useState} from "react";


import '../../pages/message-editor/style.css'
import {useSelector} from "react-redux";

function EditMessageModal({closeModal, editMessage, getMessage}) {
    const [text, setText] = useState('');
    const idEditMessage = useSelector(state => state.editMessageId);
    const editModal = useSelector(state => state.editModal);
    const message = getMessage(idEditMessage);

    useEffect(() => {
        if(idEditMessage){
            setText(message.text)
        } else {
            closeModal()
        }
    }, [idEditMessage]);


    return (
        <div className={`edit-message-modal ${editModal ? 'modal-shown': ''}`}>
            <div className="edit-message-modal__block">
                <textarea name="" id="" cols="50" rows="20" value={text} onChange={(e) => setText(e.target.value)} className="edit-message-input"></textarea>
                <div className="menu-modal">
                    <button className="edit-message-close" onClick={() => closeModal()}>CLOSE</button>
                    <button className="edit-message-button" onClick={() => editMessage(message.id, text)}>SUBMIT</button>
                </div>
            </div>
        </div>
    )
}

export default EditMessageModal;