
class Message {
  constructor({ http }) {
    this._http = http;
  }

  getMessages(){
    return this._http.load(`/api/messages`, {
      method: 'GET'
    });
  }

  getMessage(id) {
    return this._http.load(`/api/messages/${id}`, {
      method: 'GET'
    });
  }

  addMessage(payload) {
    return this._http.load('/api/messages', {
      method: 'POST',
      contentType: 'application/json',
      payload: JSON.stringify(payload)
    });
  }

  editMessage(id, payload) {
    return this._http.load(`/api/messages/${id}`, {
      method: 'PUT',
      contentType: 'application/json',
      payload: JSON.stringify(payload)
    });
  }

  deleteMessage(id) {
    return this._http.load(`/api/messages/${id}`, {
      method: 'DELETE',
      contentType: 'application/json'
    });
  }
}

export { Message };
