
class User {
  constructor({ http }) {
    this._http = http;
  }

  getUsers(){
    return this._http.load(`/api/users`, {
      method: 'GET'
    });
  }

  getUser(id) {
    return this._http.load(`/api/users/${id}`, {
      method: 'GET'
    });
  }

  addUser(payload) {
    return this._http.load('/api/users', {
      method: 'POST',
      contentType: 'application/json',
      payload: JSON.stringify(payload)
    });
  }

  editUser(id, payload) {
    return this._http.load(`/api/users/${id}`, {
      method: 'PUT',
      contentType: 'application/json',
      payload: JSON.stringify(payload)
    });
  }

  deleteUser(id) {
    return this._http.load(`/api/users/${id}`, {
      method: 'DELETE',
      contentType: 'application/json'
    });
  }
}

export { User };
