import { Auth } from './auth/auth.service';
import { Message } from './message/message.service';
import { Http } from './http/http.service';
import { Storage } from './storage/storage.service';
import { User } from './user/user.service';

const storage = new Storage({
  storage: localStorage
});

const http = new Http({
  storage
});

const auth = new Auth({
  http
});

const message = new Message({
  http
});

const user = new User({
  http
});


export { http, storage, auth, message, user};
